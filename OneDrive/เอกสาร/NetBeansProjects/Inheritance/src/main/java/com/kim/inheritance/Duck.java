/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kim.inheritance;

/**
 *
 * @author ASUS
 */
public class Duck extends Animal{
    private int numberOfWings;
    public Duck(String name, String color){
        super(name,color, 2);
    }
    public void fly(){
        System.out.println("Duck:" + name + " fly!!!");
    }
}
